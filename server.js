var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca2als/collections/movimientos?apiKey=GBbMG6TTqNUNM4iXsl04LADfJwO0Qs8b";

var clienteMLab = requestjson.createClient(urlmovimientosMlab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get("/movimientos",function(req, res){
  clienteMLab.get('', function(err, resM, body){
    if(err){
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.get("/clientes/:idcliente", function(req, res){
  res.send("Aqui tiene al cliente numero: "+req.params.idcliente);
})


app.post("/", function(req, res){
  res.send("Hemos recibido su peticiònb post actualizada");
})

app.post("/movimientos",function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})


app.put("/", function(req, res){
  res.send("Hemos recibido su peticiònb put");
})

app.delete("/", function(req, res){
  res.send("Hemos recibido su peticiònb delete");
})

app.put("/actualiza/:id", function(req, res) {
  var idcliente = req.params.id
  var query = '&q={"idcliente":'+idcliente+'}'
  clienteMlab = requestjson.createClient(urlmovimientosMlab+query)
  clienteMlab.put('', req.body, function(err, resM, body) {
      res.send(body)
  })
})
